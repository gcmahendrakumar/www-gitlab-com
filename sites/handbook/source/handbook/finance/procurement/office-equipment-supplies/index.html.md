---
layout: handbook-page-toc
title: Home Office Equipment and Supplies
---

{::options parse_block_html="true" /}

<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />

## Home Office Equipment and Supplies

Prior to your start date, you will [work with GitLab IT to acquire a laptop](https://about.gitlab.com/handbook/business-technology/team-member-enablement/onboarding-access-requests/#laptops).

As you onboard and start your Gitlab journey, new hires after 2023-02-08 will receive a 1,500 USD (or equivalent local currency) stipend to help you towards setting up your home office in your first year.  This will be issued to you as a Virtual Card, and you will be able to use that card to purchase items. The Virtual Card information will be sent to you by the Accounts Payable team via your Onboarding Issue.  Please refer to GitLab's [guide to a productive home office or remote workspace](https://about.gitlab.com/company/culture/all-remote/workspace/) as a tool to guide you in how to create the perfect home office setup for remote working. Please refer to the [Navan Expense End Users Guide](https://about.gitlab.com/handbook/business-technology/enterprise-applications/guides/navan-expense-guide/) for instructions on using and accessing your Virtual Card.

In addition, as of 2023-02-08, team members will get a yearly refresh of 500 USD (or equivalent local currency) to purchase upgrades, replacements, or enhancements to your home office setup. This excludes laptops - please connect with the IT team if you require a replacement laptop.

Team members who have started during the year before this expense policy took effect, and have not yet expensed key parts of their home office equipment (such as office chair, desk and monitor) are also eligible for the New Hire 1,500 USD stipend. In order to claim this, please email expenses@gitlab.com and explain your situation. The team will assist you with provisioning the Virtual Card. 

### New Hires- 1,500 USD (or equivalent local currency) Guidelines

1. These amounts were set after a thorough analysis and review of team members’ expense data during the previous two years and benchmarked with other remote companies.  Anything over this amount will be at the expense of the team member and no exceptions will be made.
2. The virtual card provided will expire after one year of issue.  
3. Any unused funds will not roll over to the next year.
4. The local currency FX rate will be calculated using the OANDA Currency Converter as of the date the virtual card is issued.
5. We want you to do you and set up your office the way you want it. Although we don't want to put any restrictions regarding which office equipment you may purchase, we would like to outline what is covered under the term Office Equipment. What's included - Essential items which you will need to ensure you are able to perform your role in Gitlab should include chair, desk, monitor, Laptop (provided), headset, external keyboard and external mouse. Further to this, items like additional monitors, laptop stand, external webcam, supplementary lighting can also be considered and expensed using the Stipend/Allowance. Any items outside this will fall under an exception to the policy and will be rejected. In addition the following items are not reimbursable: Airpods (they are highly discouraged due to audio quality issues & limited battery life and should not be purchased), phones, watches and tablets. If you have any doubts please connect with the Expense Team on expenses@gitlab.com. Please note that these purchases are still reviewed by your manager and accounting teams, and any misappropriation of these funds will be flagged.

### All Team Members- 500 USD (or equivalent local currency) Refresh Guidelines

1. These amounts were set after a thorough analysis and review of team members’ expense data during the previous four years and benchmarked with other remote companies.  Anything over this amount will be at the expense of the team member and no exceptions will be made.
1. This is a yearly refresh amount.  It will reset at the beginning of each fiscal year (202X-02-01 to 202Y-01-31).
1. The Navan Virtual Card cannot be used for the yearly refresh. Team members should use their own method of payment and submit their expenses in Navan. 
1. Any unused funds will not roll over to the next year. 
1. We want you to do you and set up your office the way you want it. Although we don't want to put any restrictions regarding which office equipment you may purchase, we would like to outline what is covered under the term Office Equipment. What's included - Essential items which you will need to ensure you are able to perform your role in Gitlab should include chair, desk, monitor, Laptop (provided), headset, external keyboard and external mouse. Further to this, items like additional monitors, laptop stand, external webcam, supplementary lighting can also be considered and expensed using the Stipend/Allowance. Any items outside this will fall under an exception to the policy and will be rejected. In addition the following items are not reimbursable: Airpods (they are highly discouraged due to audio quality issues & limited battery life and should not be purchased), phones, watches and tablets. If you have any doubts please connect with the Expense Team on expenses@gitlab.com. Please note that these purchases are still reviewed by your manager and accounting teams, and any misappropriation of these funds will be flagged.

For any questions about this process or about utilizing your Virtual Card (new hires only), please reach out in the [#expense-reporting-inquiries](https://gitlab.slack.com/archives/C012ALM8P29) Slack channel. 

For questions on how to submit your home office expenses, including which categories to use, please refer to the [Expenses handbook page](https://about.gitlab.com/handbook/finance/expenses/).
